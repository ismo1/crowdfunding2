module ApplicationHelper

  def app_title(title)
    title.blank? ? Settings.application_name : "#{Settings.application_name} | #{title}"
  end

# Returns the Gravatar (http://gravatar.com/) for the given user.
  def gravatar_for(user, options = { size: 60 })
    gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
    size = options[:size]
    gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?s=#{size}"
    image_tag(gravatar_url, alt: user.first_name, class: "gravatar")
  end


  def bootstrap_class_for(flash_type)
    { success: "alert-success",
      error: "alert-error",
      alert: "alert-warning",
      notice: "alert-info"
    }[flash_type] || flash_type.to_sym
  end

  def flash_messages(opts = {})
    flash.each do |msg_type, message|
      concat(content_tag(:div, message, class: "alert #{bootstrap_class_for(msg_type)} fade in") do
              concat content_tag(:button, '×'.html_safe, class: "close", data: { dismiss: 'alert' })
              concat message
            end)
    end
    nil
  end

end

# == Schema Information
#
# Table name: projects
#
#  id          :integer          not null, primary key
#  title       :string(255)
#  description :text
#  user_id     :integer
#  created_at  :datetime
#  updated_at  :datetime
#

class Project < ActiveRecord::Base
	belongs_to :user
  validates :description, presence: true
  validates :title, presence: true,  :length => {:minimum => 5, :maximum => 50}
  validates :user_id, presence: true

  scope :owner, -> (user_id) { where("user_id = ?", user_id.to_i) }
  default_scope -> { order('created_at DESC') }
end

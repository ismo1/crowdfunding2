# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  first_name      :string(255)
#  last_name       :string(255)
#  email           :string(255)
#  created_at      :datetime
#  updated_at      :datetime
#  password_digest :string(255)
#  admin :boolean, default: false
#

class User < ActiveRecord::Base
	has_many :projects, dependent: :destroy
  before_save { self.email = email.downcase }
  before_create :create_remember_token
  has_secure_password

	validates :first_name, :last_name, presence: :true, :length => {:minimum => 3, :maximum => 20}
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(?:\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :email, presence: true, format: { with: VALID_EMAIL_REGEX }, :uniqueness => { case_sensitive: false }
  validates :password, length: { minimum: 6 }, :on => :create

  def self.search(search)
    if search
      find(:all, :conditions => ['first_name LIKE ?' , "%#{search}%"])
    else
      find(:all)
    end
  end

  def pretty_name
    "#{self.first_name} #{self.last_name}"
  end

  def self.new_remember_token
    SecureRandom.urlsafe_base64
  end

  def self.digest(token)
    Digest::SHA1.hexdigest(token.to_s)
  end

  def self.digest_token
    User.digest(User.new_remember_token)
  end

  def feed
    projects
  end

private

  def create_remember_token
    self.remember_token = User.digest_token
  end

end

class UserMailer < ActionMailer::Base
  include SessionsHelper

  default from: "no-replay@crowdfunding.com"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.welcome.subject
  #
  def welcome(user)
    @user = user
    mail to: user.email, subject: "welcome #{user.first_name} !!" #, skip_premailer: false
  end
  # handle_asynchronously :welcome

  def send_email(user, message)
    @user = user
    @sender = "Webmaster"
    # @sender = @current_user if signed_in?
    @message = message
    mail to: user.email, subject: "welcome #{user.first_name} !!" #, skip_premailer: false
  end

end

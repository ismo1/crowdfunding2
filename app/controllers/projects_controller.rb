class ProjectsController < ApplicationController
  include ApplicationHelper
  before_action :set_project, only: [:show, :edit, :update, :destroy]
  before_action :signed_in_user, only: [:new, :edit, :update, :destroy]
  before_action :correct_user, only: [:edit, :update, :destroy]

  # GET /projects
  def index
    @title = app_title('Projects Feed')
    @projects = Project.owner(params[:user_id]).paginate(page: params[:page],:per_page=> 5) if params[:user_id]
    @projects ||= Project.all.paginate(page: params[:page],:per_page=> 5)
  end

  # GET /projects/1
  def show
    # @projects  = current_user.projects.build
    # @feed_items = current_user.feed.paginate(page: params[:page]) if signed_in?
  end

  # GET /projects/new
  def new
    @project = Project.new
  end

  # GET /projects/1/edit
  def edit
  end

  # POST /projects
  def create
    @project = current_user.projects.build(project_params) unless !signed_in?
    @project ||=  Project.new(project_params)

    if @project.save
      redirect_to @project, :flash => { success: 'Project was successfully created.'}
    else
      render action: 'new'
    end
  end

  # PATCH/PUT /projects/1
  def update
    if @project.update(project_params)
      redirect_to @project, :flash => { success: 'Project was successfully updated.'}
    else
      render action: 'edit'
    end
  end

  # DELETE /projects/1
  def destroy
    @project.destroy
    redirect_to user_projects_path(current_user), :flash => { success: 'Project was successfully destroyed.' }
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_project
      @project = Project.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def project_params
      params.require(:project).permit(:title, :description, :user_id)
    end

    def correct_user
      @project = Project.find(params[:id])
      redirect_to(root_url) unless current_user?(@project.user)
    end
end

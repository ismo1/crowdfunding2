class SessionsController < ApplicationController
  include ApplicationHelper
  def new
    @title = app_title('Sign in')
  end

  def create
    @title = app_title('Sign in')
    user = User.find_by_email(session_params[:email].downcase)
    if user && user.authenticate(session_params[:password])
      @title = app_title(user.pretty_name)
      sign_in(user)
      redirect_back_or current_user
    else
      flash.now[:error] = 'Invalid email/password combination'
      render 'new'
    end
  end

  def destroy
    sign_out
    flash[:success] = 'Sucess Sign Out'
    redirect_to root_url
  end

private
  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.find_by_email(params[:session][:email].downcase)
  end

  # Only allow a trusted parameter "white list" through.
  def session_params
    params.require(:session).permit(:email, :password)
  end
end

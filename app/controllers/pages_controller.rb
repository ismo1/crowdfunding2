class PagesController < ApplicationController
  include ApplicationHelper

  def home
    @title = app_title('Home')
  end

  def about
    @title = app_title('About')
  end

  def contact
    @title = app_title('Contact')
  end

  def blog
    @title = app_title('Blog')
  end

  def definition
    begin
      @definition = Dictionary.find(:first, :conditions => ['lower(term) LIKE ?', "%#{params[:term].downcase}%"])
      render :xml => @definition
    rescue Errno::ECONNREFUSED
      flash.now[:warning] = 'Hey!  Bad things happened!'
      render :template => 'errors/500', :status => 500
    end
  end
end
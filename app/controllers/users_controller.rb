class UsersController < ApplicationController
  include ApplicationHelper

  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :signed_in_user, only: [:index, :edit, :update, :destroy]
  before_action :correct_user, only: [:edit, :update]
  before_action :admin_user,     only: :destroy

  # GET /users
  def index
    @title = app_title('All users')
    @users = User.order(created_at: :desc).paginate(page: params[:page],:per_page=> 10)
  end

  # GET /users/1
  def show
    @title = app_title(@user.pretty_name)
    @projects = @user.projects.paginate(page: params[:page])
  end

  # GET /users/new
  def new
    @title = app_title('New user')
    @user = User.new
  end

  # GET /users/1/edit
  def edit
    @title = app_title('Edit user')
  end

  # POST /users
  def create
    @user = User.new(user_params)

    if @user.save
      sign_in(@user)
      redirect_to @user, :flash => { success: Settings.page_home_main_welcome_message }
      UserMailer.delay.welcome(@user)
    else
      render action: 'new'
    end
  end

  # PATCH/PUT /users/1
  def update
    if @user.update(user_params)
      redirect_to @user, :flash => { success: 'User was successfully updated.'}
    else
      render action: 'edit'
    end
  end

  # DELETE /users/1
  def destroy
    @user.destroy
    redirect_to users_url, :flash => { success: 'User was successfully destroyed.'}
  end

  def sign_up
    @title = app_title('Sign up')
    @user = User.new
  end

  private

    # Only allow a trusted parameter "white list" through.
    def user_params
      params.require(:user).permit(:first_name, :last_name, :email, :password, :password_confirmation)
    end

    # Before filters

    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end
end

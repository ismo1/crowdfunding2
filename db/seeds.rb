# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


# encoding: utf-8

# User.delete_all
# #Project.delete_all

# User.create(first_name: 'ismael',
#             last_name: 'Moussa',
#             email: 'ismo1@live.fr')

# User.create(first_name: 'ismo1',
#             last_name: 'Issy',
#             email: 'issy@live.fr')

# first_user ||= User.first
# last_user ||= User.last

# Project.create(title: 'Project 1',
#                description: %{<p>
#                 <em>Rails Test Prescriptions</em> is a comprehensive guide to testing
#                 Rails applications, covering Test-Driven Development from both a
#                 including Cucumber, Shoulda, Machinist, Mocha, and Rcov.
#                 </p>},
#                 user_id: first_user)

# Project.create(title: 'Project 2',
#                description: %{<p>
#                 <em>Rails Test Prescriptions</em> is a comprehensive guide to testing
#                 Rails applications, covering Test-Driven Development from both a
#                 including Cucumber, Shoulda, Machinist, Mocha, and Rcov.
#                 </p>},
#                 user_id: last_user)
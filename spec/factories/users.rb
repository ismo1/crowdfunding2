# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  first_name      :string(255)
#  last_name       :string(255)
#  email           :string(255)
#  created_at      :datetime
#  updated_at      :datetime
#  password_digest :string(255)
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user do
    first_name  { Faker::Name.first_name + "_first" }
    last_name   { Faker::Name.last_name + "_last" }
    email       { Faker::Internet.email }
    password "password"
    password_confirmation "password"

    factory :admin do
      admin true
    end
  end
end

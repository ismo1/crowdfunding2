require 'spec_helper'
include ApplicationHelper

describe 'User' do

  let(:user) { FactoryGirl.create(:user) }

  subject { page }
  brand = Settings.application_name

  describe "Index" do
    before(:each) do
      FactoryGirl.create(:user)
      FactoryGirl.create(:user)
      FactoryGirl.create(:user)
      user = FactoryGirl.create(:user)
      sign_in(user)
    end

    describe "Page" do
      before { visit users_path }

      it { should have_title(app_title("All users")) }

      it "Shoul list All users" do
        User.all.each do |user|
          expect(page).to have_selector('td', text: user.first_name)
          expect(page).to have_selector('td', text: user.last_name)
        end
      end
    end

    describe "pagination" do
      before(:all) { 30.times { FactoryGirl.create(:user) } }
      before { visit users_path }
      after(:all)  { User.delete_all }

      it { should have_selector('div.pagination') }

      # it "should list each user" do
      #   User.paginate(page: 1).each do |user|
      #     expect(page).to have_content(user.first_name)
      #   end
      # end
    end
  end

  describe "delete links" do

    it { should_not have_link('delete') }

    describe "as an admin user" do
      let(:admin) { FactoryGirl.create(:admin) }
      before do
        sign_in(admin)
        visit users_path
      end

      # it { should have_selector('i') }
      # it "should be able to delete another user" do
      #   expect do
      #     click_link('i', match: :first)
      #   end.to change(User, :count).by(-1)
      # end
      # it { should_not have_link('delete', href: user_path(admin)) }
    end
  end

  describe "Sign up Page" do

    let(:valid_attributes) { attributes_for(:user) }
    let(:submit) { "Create User" }
    before { visit sign_up_path }

    it { should have_content(brand) }
    it { should have_title(app_title("Sign up")) }
    it { should have_selector('h1', :text => 'Sign up') }
    it { click_button "Create User" }

    context "When Sign up with invalid data" do
      it "should not create a user" do
        expect { click_button submit }.not_to change(User, :count)
      end

      it "should have a flash error with selector 'alert' & content 'The form contains..'" do
        click_button submit
        expect(page).to have_selector('.alert-error', :text => 'The form contains')
      end
    end

    context "When Sign up with valid data" do
      before do
        fill_in "First name",     with: "First_first_name"
        fill_in "Last name",      with: "Last_last_name"
        fill_in "Email",          with: "first_last_name@email.com"
        fill_in "Password",       with: "pass_password"
        fill_in "Confirmation",   with: "pass_password"
      end

      it "should create a user" do
        expect { click_button(submit) }.to change(User, :count).by(1)
      end

      it "should have a flash succes with selector 'alert' & content 'Welcome..'" do
        click_button submit
        expect(page).to have_success_message(Settings.page_home_main_welcome_message)
      end

      it "should retrive the new user with find_by_email" do
        click_button submit
        user =  User.find_by_email("first_last_name@email.com")
        expect(page).to have_title(user.pretty_name)
      end
    end
  end

  describe "Profile page" do
    let(:user) { FactoryGirl.create(:user) }
    let!(:p1)  { FactoryGirl.create(:project, user: user, title: "Fooooo") }
    let!(:p2)  { FactoryGirl.create(:project, user: user, title: "Baaaar") }

    before { visit user_path(user) }
    # it { should have_selector("h1", :text => user.first_name) }
    it { should have_title(app_title(user.pretty_name)) }
    it { should have_content(user.first_name) }
    # it { should have_content(user.last_name) }
    # it { should have_content(user.email) }

    describe "project" do
      it { should have_content(p1.title) }
      it { should have_content(p2.title) }
      it { should have_content(user.projects.count) }
    end

  end

  describe "Edit " do
    let(:submit) { "Update User" }
    let(:user) { FactoryGirl.create(:user) }

    before do
      sign_in(user)
      visit edit_user_path(user)
    end

    describe "page" do
      before { visit edit_user_path(user) }

      it { should have_title app_title("Edit user") }
      it { should have_selector("h1", :text => "Update your profile") }
      it { should have_link('Change', href: 'http://gravatar.com/emails') }
    end

    describe "submit with invalid information" do
      before do
        fill_in "Password",       with: "pass"
        click_button "Update User"
      end
      it { should have_error_message('The form contains') }
    end

    describe "with valid information" do

      let(:new_name)  { "New Name" }
      let(:new_email) { "new@example.com" }

      before do
        fill_in "First name",       with: new_name
        fill_in  "Email",           with: new_email
        fill_in "Password",         with: user.password
        fill_in "Confirmation",     with: user.password
        click_button "Update User"
      end

      it { should have_title(app_title(new_name)) }
      it { should have_success_message('') }
      # it { should have_link(' Sign out', href: '/sign_out') }
      specify { expect(user.reload.first_name).to  eq new_name }
      specify { expect(user.reload.email).to eq new_email }
    end
  end

  describe "status" do
    let!(:older_project) do
      FactoryGirl.create(:project, user: user, created_at: 1.day.ago)
    end
    let!(:newer_project) do
      FactoryGirl.create(:project, user: user, created_at: 1.hour.ago)
    end

    let(:unwatched_project) do
      FactoryGirl.create(:project, user: FactoryGirl.create(:user))
    end

    # its(:feed) { should include(newer_project) }
    # its(:feed) { should include(older_project) }
    # its(:feed) { should_not include(unwatched_project) }
  end
end
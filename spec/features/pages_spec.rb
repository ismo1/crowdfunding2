require 'spec_helper'

include ApplicationHelper

describe "Pages" do
  subject { page }

  brand = Settings.application_name
  describe "Home page" do

    before { visit home_path }

    it { should have_content(brand) }
    it { should have_title(app_title("Home")) }
    it { should have_selector('h1', :text => Settings.page_home_main_welcome_message) }

  end

  describe "About page" do

    before { visit about_path }

    it { should have_content(brand) }
    it { should have_title(app_title("About")) }
    it { should have_selector('h1', :text => 'About') }

  end

  describe "Contact page" do

    before { visit contact_path }

    it { should have_content(brand) }
    it { should have_title(app_title("Contact")) }
    it { should have_selector('h1', :text => 'Contact') }

  end

  describe "Blog page" do

    before { visit blog_path }

    it { should have_content(brand) }
    it { should have_title(app_title("Blog")) }
    it { should have_selector('h1', :text => 'Blog') }
  end

  describe "Pages Links" do

    context "When click_link 'Home'" do
      before { visit home_path }
      it "should have_title with content' | Home'" do
        click_link 'Home'
        expect(page).to have_title("#{brand} | Home")
      end
    end

    context "When click_link 'CROWDFUNDING'" do
      before { visit home_path }
      it "should have_title with content' | Home'" do
        click_link "Crowdfunding"
        expect(page).to have_title("#{brand} | Home")
      end
    end

    context "When click_link 'About'" do
      before { visit about_path }
      it "should have_title with content' | About'" do
        within(".container-top-navbar") do
          click_on("About")
        end
        expect(page).to have_title("#{brand} | About")
      end
    end

    context "When click_link 'Contact'" do
      before { visit contact_path }
      it "should have_title with content' | Contact'" do
        within(".container-top-navbar") do
          click_on("Contact")
        end
        expect(page).to have_title("#{brand} | Contact")
      end
    end

    context "When click_link 'Blog'" do
      before { visit blog_path }
      it "should have_title with content' | Blog'" do
        click_link "Blog"
        expect(page).to have_title("#{brand} | Blog")
      end
    end
  end

end
require 'spec_helper'
include ApplicationHelper

describe  "Authentication", type: :request do
  subject { page }

  brand = Settings.application_name

  describe "Sign in Page" do
    before { visit sign_in_path }
    let(:submit) { "Sign in" }

    it { should have_selector("h1", :text => "Sign in")}
    it { should have_title(app_title("Sign in")) }
    it { should have_content(brand) }

    context "When Sign in with invalid data" do

      it "should have a flash error with selector 'alert' & content 'Invalid email..'" do
        click_button submit
        expect(page).to have_selector('.alert-error', :text => 'Invalid email')
      end

      describe "after visiting another page" do
        it "Shoul not have the last flash error with selector 'alert'" do
          visit home_path
          expect(page).to_not have_selector('.alert-error', :text => 'Invalid email')
        end
      end
    end

    context "When Sign in with valid data" do
      let(:user) { FactoryGirl.create(:user) }
      before do
        fill_in "Email",     with: user.email.upcase
        fill_in "Password",  with: user.password
        click_button "Sign in"
      end

      it { should have_title(app_title(user.pretty_name)) }
      it { should have_link('Projects',      href:  user_projects_path(user)) }
      it { should have_link('Profile',       href:  user_path(user)) }
      it { should have_link('Settings',      href: edit_user_path(user)) }
      it { should_not have_link('Sign in',   href: sign_in_path) }
      it { should have_link('Sign out',      href: sign_out_path) }

    end

    context "When Sign out" do
      let(:user) { FactoryGirl.create(:user) }
      before do
        fill_in "Email",     with: user.email.upcase
        fill_in "Password",  with: user.password
        click_button "Sign in"
        click_link 'Sign out'
      end

      it { should have_link('Sign in',   href: sign_in_path) }

    end
  end

  describe "authorization" do

    describe "for non-signed-in users" do
      let(:user) { FactoryGirl.create(:user) }

      describe "in the Users controller" do

        describe "visiting the edit page" do
          before { visit edit_user_path(user) }
          it { should have_title('Sign in') }
        end

        describe "submitting to the update action" do
          before { patch user_path(user) }
          specify { expect(response).to redirect_to(sign_in_path) }
        end
      end

      describe "when attempting to visit a protected page" do
        before do
          visit edit_user_path(user)
          fill_in "Email",    with: user.email
          fill_in "Password", with: user.password
          click_button "Sign in"
        end

        describe "after signing in" do

          it "should render the desired protected page" do
            expect(page).to have_title('Edit user')
          end
        end
      end
    end

    describe "as wrong user" do
      let(:user) { FactoryGirl.create(:user) }
      let(:wrong_user) { FactoryGirl.create(:user, email: "wrong@example.com") }
      before do
        sign_in(wrong_user)
      end

      describe "submitting a GET request to the Users#edit action" do
        before { get edit_user_path(user) }
        specify { expect(response.body).not_to match(app_title('Edit user')) }
        # specify { expect(response).to redirect_to(root_path) }
      end

      describe "submitting a PATCH request to the Users#update action" do
        before { patch user_path(user) }
        # specify { expect(response).to redirect_to(root_path) }
      end
    end

    # describe "in the Projects controller" do

    #   describe "submitting to the create action" do
    #     before { post projects_path }
    #     specify { expect(response).to redirect_to(signin_path) }
    #   end

    #   describe "submitting to the destroy action" do
    #     before { delete projects_path(FactoryGirl.create(:project)) }
    #     specify { expect(response).to redirect_to(signin_path) }
    #   end
    # end
  end
end
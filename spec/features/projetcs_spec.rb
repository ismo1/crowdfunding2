require 'spec_helper'
include ApplicationHelper

describe 'Project' do

  let(:project) { FactoryGirl.create(:project) }

  subject { page }
  brand = Settings.application_name

  describe "Index", :focus do
    before(:each) do
      user = FactoryGirl.create(:user)
      FactoryGirl.create(:project, user: user)
      FactoryGirl.create(:project, user: user)
      FactoryGirl.create(:project)
      sign_in(user)
    end

    # describe "Page" do
    #   before { visit projects_path }
    #   it { should have_title(app_title("Projects Feed")) }
    # end


    # describe "for signed-in users" do
    #   let(:user) { FactoryGirl.create(:user) }
    #   before do
    #     FactoryGirl.create(:project, user: user)
    #     FactoryGirl.create(:project, user: user)
    #     sign_in user
    #     visit projects_path
    #   end

    #   it "should render the user's feed" do
    #     user.feed.each do |item|
    #       expect(page).to have_selector("li##{item.id}", text: item.titele)
    #     end
    #   end
    # end
  end
end
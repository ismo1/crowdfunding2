require 'spec_helper'
require 'support/utilities'

describe PagesController do

  context 'Pages simple' do

    describe "GET 'home'" do

      it "returns http success" do
        get :home
        response.should be_success
      end
    end

    describe "GET 'contact'" do

      it "returns http success" do
        get :contact
        response.should be_success
      end
    end

    describe "GET 'about'" do

      it "returns http success" do
        get :about
        response.should be_success
      end
    end

    describe "GET 'blog '" do

      it "returns http success" do
        get :blog
        response.should be_success
      end
    end
  end
end

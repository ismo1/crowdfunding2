require 'spec_helper'

describe UsersController  do

  before(:each) do
    @user = FactoryGirl.create(:user)
    sign_in(@user, no_capybara: true)
  end

  let(:valid_attributes) { attributes_for(:user) }

  describe "GET index" do
    it "assigns all users as @users" do
      get :index
      assigns(:users).should eq([@user])
    end
  end

  describe "GET show" do
    it "assigns the requested user as @user" do
      get :show, {:id => @user.to_param }
      assigns(:user).should eq(@user)
    end
  end

  describe "GET new" do
    it "assigns a new user as @user" do
      get :new
      assigns(:user).should be_a_new(User)
    end
  end

  describe "GET edit" do
    before { sign_in(@user, no_capybara: true) }
    it "assigns the requested user as @user" do
      get :edit, {:id => @user.to_param }
      assigns(:user).should eq(@user)
    end
  end

  describe "POST create" do

    describe "with valid params" do
      it "creates a new User" do
        expect {
          post :create, {:user => valid_attributes }
        }.to change(User, :count).by(1)
      end

      it "assigns a newly created user as @user" do
        post :create, {:user => valid_attributes }
        assigns(:user).should be_a(User)
        assigns(:user).should be_persisted
      end

      it "redirects to the created user" do
        post :create, {:user => valid_attributes }
        response.should redirect_to(User.last)
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved user as @user" do
        # Trigger the behavior that occurs when invalid params are submitted
        User.any_instance.stub(:save).and_return(false)
        post :create, {:user => { "first_name" => "invalid value" }}
        assigns(:user).should be_a_new(User)
      end

      it "re-renders the 'new' template" do
        # Trigger the behavior that occurs when invalid params are submitted
        User.any_instance.stub(:save).and_return(false)
        post :create, {:user => { "first_name" => "invalid value" }} #, valid_session
        response.should render_template("new")
      end
    end
  end

  describe "PUT update"  do
    before { sign_in(@user, no_capybara: true) }
    describe "with valid params" do
      it "updates the requested user" do
        User.any_instance.should_receive(:update).with({ "first_name" => "MyString" })
        put :update, {:id => @user.to_param, :user => { "first_name" => "MyString" }} #, valid_session
      end

      it "assigns the requested user as @user" do
        put :update, {:id => @user.to_param, :user => valid_attributes} #, valid_session
        assigns(:user).should eq(@user)
      end

      it "redirects to the user" do
        put :update, {:id => @user.to_param, :user => valid_attributes} #, valid_session
        response.should redirect_to(@user)
      end
    end

    describe "with invalid params" do
      before { sign_in(@user, no_capybara: true) }
      it "assigns the user as @user" do
        # Trigger the behavior that occurs when invalid params are submitted
        User.any_instance.stub(:save).and_return(false)
        put :update, {:id => @user.to_param, :user => { "first_name" => "invalid value" }} #, valid_session
        assigns(:user).should eq(@user)
      end

      it "re-renders the 'edit' template" do
        # Trigger the behavior that occurs when invalid params are submitted
        User.any_instance.stub(:save).and_return(false)
        put :update, {:id => @user.to_param, :user => { "first_name" => "invalid value" }} #, valid_session
        response.should render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do
    before do
      admin = FactoryGirl.create(:admin)
      sign_in(admin)

    end

  #   it "destroys the requested user" do
  #     expect {
  #       delete :destroy, {:id => @user.to_param} #, valid_session
  #     }.to change(User, :count).by(-1)
  #   end

  #   it "redirects to the users list" do
  #     delete :destroy, {:id => @user.to_param} #, valid_session
  #     response.should redirect_to(users_url)
  #   end
  end

  describe "GET 'signup '" do
    it "returns http success" do
      get :sign_up
      response.should be_success
    end
  end
end

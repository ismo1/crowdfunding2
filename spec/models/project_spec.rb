# == Schema Information
#
# Table name: projects
#
#  id          :integer          not null, primary key
#  title       :string(255)
#  description :text
#  user_id     :integer
#  created_at  :datetime
#  updated_at  :datetime
#

require 'spec_helper'

describe Project do

  let(:user) { FactoryGirl.create(:user) }
  let(:project) { user.projects.build(attributes_for(:project)) }

  subject { project }

  it { should be_valid }

  describe "Project's owner" do
    it { project.user should_not be_nil }
    it { should respond_to(:user_id) }
    it { should respond_to(:user) }
    its(:user) { should eq user }
  end

  describe "when user_id is not present" do
    before { project.user_id = nil }
    it { should_not be_valid }
  end

  describe "#title" do
    it { should respond_to(:title) }
    context "When is not present or blank" do
      before { project.title = " " }
      it { should_not be_valid }
    end

    context "When is too short" do
      before { project.title = "***" }
      it { should_not be_valid }
    end

    context "When is too short" do
      before { project.title = "***" * 30}
      it { should_not be_valid }
    end
  end

  describe "#description" do
    it { should respond_to(:description) }
    context "When is not present or blank" do
      before { project.description = " " }
      it { should_not be_valid }
    end
  end

end

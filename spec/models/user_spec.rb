# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  first_name      :string(255)
#  last_name       :string(255)
#  email           :string(255)
#  created_at      :datetime
#  updated_at      :datetime
#  password_digest :string(255)
#

require 'spec_helper'

describe User do

  let(:user) { FactoryGirl.create(:user)}
  let(:user2) { FactoryGirl.create(:user)}
  subject { user }

  it { should be_valid }

  it { should respond_to(:authenticate) }
  it { should respond_to(:admin) }
  it { should respond_to(:projects) }

  it { should be_valid }
  it { should_not be_admin }

  describe "with admin attribute set to 'true'" do
    before do
      user.save!
      user.toggle!(:admin)
    end

    it { should be_admin }
  end

  describe "#first_name" do
    it { should respond_to (:first_name) }
    context "When is not present or blank" do
      before { user.first_name = "" }
      it { should_not be_valid }
    end

    context "When is too short" do
      before { user.first_name = "**" }
      it { should_not be_valid }
    end

    context "When is too long" do
      before { user.first_name = "**" * 20 }
      it { should_not be_valid }
    end
  end

  describe "#last_name" do
    it { should respond_to (:last_name) }
    context "When is not present or blank" do
      before { user.last_name = "" }
      it { should_not be_valid }
    end

    context "When is too short" do
      before { user.last_name = "**" }
      it { should_not be_valid }
    end

    context "When is too long" do
      before { user.last_name = "**" * 20 }
      it { should_not be_valid }
    end
  end

  describe "#email" do
    it { should respond_to (:email) }
    context "When is not present or blank" do
      before { user.email = "" }
      it { should_not be_valid }
    end

    context "When is not valid email" do
      before { user.email = "email.com" }
      it { should_not be_valid }
    end

    context "when email address is already taken" do
      let(:user_with_same_email) { user.dup }
      it " should validate_uniqueness_of email " do
        user_with_same_email.email = user.email.upcase
        user_with_same_email.save
        expect(user_with_same_email).not_to be_valid
      end

      it { should validate_uniqueness_of (:email) }

    end

    context "When email is saving, it must be in downcase format" do
      let(:test_email) { "Test@EMAIL.coM" }
      before do
        user.email = test_email
        user.save
      end

      it "Shoud not be eq to 'Test@EMAIL.coM'" do
        expect(user.email).not_to eq test_email
      end

      it "Shoud be eq to test@email.com" do
        expect(user.reload.email).to eq test_email.downcase
      end
    end
  end


  describe "#password_digest" do
    it { should respond_to(:password_digest) }
  end

  describe "#remember_token" do
    it { should respond_to(:remember_token) }
    before { user.save }
    its(:remember_token) { should_not be_blank }
  end

  describe "#password & #password_confirmation" do
    it { should respond_to(:password) }
    it { should respond_to(:password_confirmation) }

    # context "When password is too short" do
    #   before { user.password = user.password_confirmation = "a" * 5 }
    #   it { should be_invalid }
    # end


    context "When password match to password_cofirmation" do
      it { should be_valid }
    end

    context "When password does not present" do
      before { user.password = "", user.password_confirmation = "" }
      it { should be_invalid }
    end

    context "When password does not match password_confirmation" do
      before { user.password_confirmation = "invalid_#{user.password}" }
      it { should be_invalid }
    end
  end

  describe "Authenticate : the return value of authenticate method " do
    before { user.save }
    it { should respond_to(:authenticate) }

    let(:found_user) { User.find_by(email: user.email) }

    context "With valid password" do
      it { should eq found_user.authenticate(user.password) }
    end

    context "With invalid password" do
      let(:user_for_invalid_password) { found_user.authenticate("invalid_#{user.password}") }
      it { should_not eq user_for_invalid_password }
      specify { expect { expect(user_for_invalid_password).to be_false } }
    end
  end

  # describe "Destroying", :focus do
  #   let(:user2) { FactoryGirl.create(:user)}
  #   before do
  #     user.save!
  #     user.toggle!(:admin)
  #     # sign_in(admin)

  #     user2.projects.create!(attributes_for(:project))
  #     user2.projects.create!(attributes_for(:project))
  #     user2.projects.create!(attributes_for(:project))
  #   end

  #   context "When User is deleted" do
  #     it "should destroy associated projects" do

  #       p projects = @user.projects.to_a

  #       @user.destroy
  #       expect(projects).not_to be_empty
  #       projects.each do |project|
  #         expect(Project.where(id: project.id)).to be_empty
  #       end
  #     end
  #   end
  # end

end

Crowdfunding::Application.routes.draw do
  get "/home",     to: 'pages#home',      as: 'home'
  get "/contact",  to: 'pages#contact',   as: 'contact'
  get "/about",    to: 'pages#about',     as: 'about'
  get "/blog",     to: 'pages#blog',      as: 'blog'


  resources :sessions, :only => [:new, :create, :destroy]

  get    "/sign_up",  to: 'users#sign_up',       as: 'sign_up'
  get    "/sign_in",  to: 'sessions#new',        as: 'sign_in'
  delete "/sign_out", to: 'sessions#destroy',    as: 'sign_out'

  get "pages/definition"

  resources :projects
  resources :users do
    resources :projects
  end

  root :to => "pages#home"

  #Route use by madrill-rails gem for webhook implementation
  resource :inbox, :controller => 'inbox', :only => [:show,:create]

  get '*unmatched_route', :to => 'application#raise_not_found!'
  get ':not_found' => redirect('/'), :constraints => { :not_found => /.*/ }

end
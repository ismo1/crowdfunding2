# https://devcenter.heroku.com/articles/rails-unicorn

if ENV['RAILS_ENV'] == 'production'
  worker_processes (ENV['WEB_CONCURRENCY'] || 3).to_i
else
  worker_processes (ENV['WEB_CONCURRENCY'] || 1).to_i
end

#port
#listen "127.0.0.1:8181", :tcp_nopush => true

timeout (ENV['WEB_TIMEOUT'] || 5).to_i
preload_app true

before_fork do |server, worker|
  Signal.trap 'TERM' do
    puts 'Unicorn master intercepting TERM and sending myself QUIT instead'
    Process.kill 'QUIT', Process.pid
  end

  if defined? ActiveRecord::Base
    ActiveRecord::Base.connection.disconnect!
  end

  if defined?(Resque)
    Resque.redis.quit
    Rails.logger.info('Disconnected from Redis')
  end
end

after_fork do |server, worker|
  Signal.trap 'TERM' do
    puts 'Unicorn worker intercepting TERM and doing nothing. Wait for master to sent QUIT'
  end

  if defined? ActiveRecord::Base
    config = Rails.application.config.database_configuration[Rails.env]
    config['reaping_frequency'] = (ENV['DB_REAPING_FREQUENCY'] || 10).to_i
    config['pool'] = (ENV['DB_POOL'] || 2).to_i
    ActiveRecord::Base.establish_connection(config)
  end

  if defined?(Resque)
    Resque.redis = ENV['REDIS_URI']
    Rails.logger.info('Connected to Redis')
  end
end

# # --- Start of unicorn worker killer code ---
# if ENV['RAILS_ENV'] == 'production'
#   require 'unicorn/worker_killer'

#   max_request_min =  500 #a worker is killed if it has handled between 500 to 600 requests
#   max_request_max =  600

#   # Max requests per worker
#   use Unicorn::WorkerKiller::MaxRequests, max_request_min, max_request_max

#   oom_min = (240) * (1024**2) #a worker is killed if it consumes between 240 to 260 MB of memory.
#   oom_max = (260) * (1024**2)

#   # Max memory size (RSS) per worker
#   use Unicorn::WorkerKiller::Oom, oom_min, oom_max
# end

# # --- End of unicorn worker killer code ---

# require ::File.expand_path('../config/environment',  __FILE__)
# run Crowdfunding::Application

require Rails.root.join('config/initializers/smtp')
require_relative 'production'

Mail.register_interceptor RecipientInterceptor.new(ENV['EMAIL_RECIPIENTS'])

Crowdfunding::Application.configure do
  # ...

  config.action_mailer.default_url_options = { host: 'staging.Crowdfunding.com' }
end

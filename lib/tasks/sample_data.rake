
namespace :db do
  desc "( rake db:populate ) Drop and create the current database and adding somes example of users & projetcs"
  task populate: :environment do
    require 'populator'
    require 'faker'

     puts "Startitng rails populate !!!!"
     puts "Create Users.."
     make_users
     puts "Create Users's Projects ...."
     make_projects
     puts "Create Admin !!!"
     make_projects_for("ismo1@live.fr")
     puts "End of tasks db is rady !!!"
  end
end

def make_users
  user = User.find_by_email("ismo1@live.fr")
  user.destroy unless user.nil?

  user = User.find_by_email("issy@rails.fr")
  user.destroy unless user.nil?

  30.times do |n|
    user = User.find_by_email("user_n-#{n+1}@rails.fr")
    user.destroy unless user.nil?
    user = User.where("email like ?", "%#{n+1}@rails.fr")
    user.destroy_all unless user.nil?
  end

  email_regex = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

  30.times do |n|
    first_name = Faker::Name.first_name
    last_name = Faker::Name.last_name
    password = Faker::Internet.password

    name_for_email = "#{first_name} #{last_name}".split.join << "#{n}@rails.fr"
    email = (email_regex.match(name_for_email))?  name_for_email : "user_n-#{n+1}@rails.fr"
     # password  = "password"
     User.create!(first_name: first_name,
                  last_name: last_name,
                  email: email,
                  password: password,
                  password_confirmation: password)
  end

  first_name = "ismael"
  last_name = "Moussa S."
  password = "ismael"

  email = "ismo1@live.fr"
  User.create!(first_name: first_name,
                last_name: last_name,
                email: email,
                password: password,
                password_confirmation: password,
                admin: true)
end

def make_projects
  User.all.order(id: :asc).limit(20).each do |user|
    content = Faker::Lorem.paragraph
    title = Faker::Name.title
    Project.create(title: title,
                   description: content,
                   user_id: user.id)
  end
end

def make_projects_for(email)
  user = User.find_or_create_by(email: email)
  3.times do
    content = Faker::Lorem.paragraph
    title = Faker::Name.title
    Project.create(title: title,
                   description: content,
                   user_id: user.id)
  end
end